<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_favorites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_usercode');
            $table->integer('favorites_usercode');
            $table->timestamps();

            $table->foreign('users_usercode')->references('usercode')->on('users');
            $table->foreign('favorites_usercode')->references('usercode')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_favorites');
    }
}
