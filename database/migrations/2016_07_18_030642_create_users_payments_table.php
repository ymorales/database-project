<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_usercode');
            $table->integer('payments_paycode');
            $table->timestamps();

            $table->foreign('users_usercode')->references('usercode')->on('users');
            $table->foreign('payments_paycode')->references('paycode')->on('payments');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_payments');
    }
}
