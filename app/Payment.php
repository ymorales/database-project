<?php

namespace App;
use Validator;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = 'payments';

    private $rules = array(
        'amount' => 'required|numeric|min:1',
        'created_at' => 'required|min:',
        // .. more rules here ..
    );

    public function payments()
    {
        return $this->belongsToMany('App\User');
    }

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            return false;
        }

        // validation pass
        return true;
    }
}
