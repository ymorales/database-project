<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class User extends Model
{
    protected $table = 'users';

    private $rules = array(
        'age' => 'required|numeric|min:18',
        'username' => 'required',
        // .. more rules here ..
    );

    public function payments()
    {
        return $this->belongsToMany('App\Payment', 'users_payments',
            'users_usercode','payments_paycode');
    }

    public function favorites()
    {
        return $this->belongsToMany('App\User', 'users_favorites',
            'users_usercode','favorites_usercode');
    }

    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            return false;
        }

        // validation pass
        return true;
    }

   
}
