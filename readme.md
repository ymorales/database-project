## HOW TO TEST VIA LARAVEL CONSOLE

```
$git clone https://ymorales@bitbucket.org/ymorales/database-project.git
$cd database-project
$touch database/database.sqlite
$composer install
$php artisan migrate

$php artisan tinker
>>> $user = new App\User()
>>> $user->save()
>>> $data = ['username'=>'my_username', 'password' => '123456', 'age'=>17]
>>> $user->validate()
>>> $data = ['username'=>'my_username', 'password' => '123456', 'age'=>18]
>>> $data->save()

```